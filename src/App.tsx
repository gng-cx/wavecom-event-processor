import React from "react";
import "./scss/main.scss";
import { useState, useEffect } from "react";
import { DATASET_URL } from "./Dataset";
import Dataset from "./Dataset";
import Logo from "./assets/logos/wavecomLogo.png";
import { useSelector, useDispatch } from "react-redux";
import { addtoData } from "./redux-store/Slices/dataSlice";
import { WritableDraft } from "immer/dist/types/types-external";
import { DatasetValues } from "./Interfaces";

function App() {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [fadeOut, setFadeOut] = useState<boolean>(false);
  const dispatch = useDispatch();
  const events = useSelector((state: WritableDraft<any>) => state.data.value);

  const fetchServerData = async () => {
    setIsLoading(true);
    try {
      const response = await fetch(
        "http://wmservices.wavesys.pt:5235/challenge/events"
      );
      const data = await response.json();

      dispatch(addtoData(data.data));
    } catch (error) {
      console.log(error);
    }
    setTimeout(() => setFadeOut(true), 4000);
    setTimeout(() => setIsLoading(false), 5000);
  };

  useEffect(() => {
    fetchServerData();
  }, []);

  return (
    <>
      <div className="App">
        <header className="App-header">
          <h1 className="text-bold large-text">Next Events</h1>
          <div className="line-events"></div>
        </header>
        <section className="events">
          {isLoading ? (
            <span
              className={fadeOut ? "loading-text fade-out" : "loading-text"}
            >
              <img className="logo-image" src={Logo} alt="logo" />
              <h1 data-text="loading..." className="text-bold normal-text">
                loading...
              </h1>
            </span>
          ) : (
            <ul className="fade-in">
              {events.map((value: DatasetValues, index: number) => {
                return (
                  <li key={index}>
                    <Dataset index={index} params={value} />
                  </li>
                );
              })}
            </ul>
          )}
        </section>
      </div>
    </>
  );
}

export default App;
