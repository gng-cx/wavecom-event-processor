import React from "react";
import { render, screen, cleanup } from "@testing-library/react";
import Dataset from "./Dataset";

import store from "./redux-store/store";
import { Provider } from "react-redux";

afterEach(() => {
  cleanup();
});

test("Dataset button should have -Mark as Seen- text", () => {
  const dataParams = {
    name: "test",
    time: "2020-09-16 13:50:50+00:00",
    description: "Event Test",
    read: false,
  };
  render(
    <Provider store={store}>
      <Dataset index={1} params={dataParams} />
    </Provider>
  );
  const datasetElement = screen.getByTestId("dataset-btn-1");
  expect(datasetElement).toBeInTheDocument();
  expect(datasetElement).toHaveTextContent("Mark as Seen");
});

test("Dataset button should have -Seen- text", () => {
  const dataParams = {
    name: "test2",
    time: "2021-09-16 13:50:50+00:00",
    description: "Event Test1",
    read: true,
  };
  render(
    <Provider store={store}>
      <Dataset index={2} params={dataParams} />
    </Provider>
  );
  const datasetElement = screen.getByTestId("dataset-btn-2");
  expect(datasetElement).toBeInTheDocument();
  expect(datasetElement).toHaveTextContent("Seen");
});
