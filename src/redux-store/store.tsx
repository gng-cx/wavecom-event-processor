import { configureStore } from "@reduxjs/toolkit";
import dataReducer from "./Slices/dataSlice";

export default configureStore({
  reducer: { data: dataReducer },
});
