import { createSlice } from "@reduxjs/toolkit";
import { DatasetValues } from "../../Interfaces";

export const dataSlice = createSlice({
  name: "data",
  initialState: {
    value: Array<DatasetValues>(),
  },
  reducers: {
    addtoData: (state, action) => {
      state.value = action.payload;
    },
    updateRead: (state, action) => {
      state.value[action.payload].read = true;
    },
  },
});

// Action creators are generated for each case reducer function
export const { addtoData, updateRead } = dataSlice.actions;

export default dataSlice.reducer;
