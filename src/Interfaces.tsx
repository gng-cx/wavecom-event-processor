export interface DatasetValues {
  name: string;
  time: string;
  description: string;
  read: boolean;
}

export interface Iparams {
  index: number;
  params: DatasetValues;
}
