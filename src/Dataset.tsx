import React from "react";
import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { updateRead } from "./redux-store/Slices/dataSlice";

import { Iparams } from "./Interfaces";
import { WritableDraft } from "immer/dist/types/types-external";

const Dataset: React.FC<Iparams> = ({ index, params }) => {
  const dispatch = useDispatch();
  const data = useSelector((state: WritableDraft<any>) => state.data.value);
  const { name, time, description, read } = params;

  const changeReadProperty = () => {
    dispatch(updateRead(index));
  };

  return (
    <article
      className={
        read
          ? "event event-read text-regular normal-text"
          : "event event-unread text-regular normal-text"
      }
    >
      <div className="event-title">
        <h3>{name}</h3>
        <h5>{time}</h5>
      </div>
      <button
        data-testid={`dataset-btn-${index}`}
        className={
          read ? "btn-event btn-event-read" : "btn-event btn-event-unread"
        }
        onClick={() => {
          changeReadProperty();
        }}
      >
        {read ? "Seen" : "Mark as Seen"}
      </button>
      <p
        className={
          read
            ? "text-regular small-text event-description"
            : "text-regular small-text event-description show"
        }
      >
        {description}
      </p>
    </article>
  );
};

export default Dataset;

export const DATASET_URL =
  "https://jsoneditoronline.org/#left=cloud.9c9d1692765e493081c3af6159354ad7";
